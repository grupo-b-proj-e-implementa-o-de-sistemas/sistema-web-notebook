import { useState } from "react";
import "../styles/loginForm.css";
// import Footer from "./Footer";
//Pacote para notificações no sistema
import { useSnackbar } from "notistack";
import api from "../api/api";
import { BsEye, BsEyeSlash } from "react-icons/bs";
import { useContext } from "react";
import { User } from "../Context/UserContext";
import { useHistory } from "react-router-dom";

//Class do botão de cadastro
let btnCadastro = "card-form-button button-ghost";

export default function LoginForm({ openLogin, setOpenLogin }) {
  const history = useHistory();
  const [cadastrarDisabled, setCadastroDisabled] = useState(true);
  //Variavel utilizado para checagem
  const [userDetails, setUserDetails] = useState({
    email: "",
    password: "",
  });
  const [visiblePassword, setVisiblePassword] = useState(false);

  //Context
  const { setLoading, setUser, setIsAdmin, setIsLoggedIn, clearNotification } =
    useContext(User);

  //Notificação
  const { enqueueSnackbar } = useSnackbar();

  if (cadastrarDisabled) {
    btnCadastro = "card-form-button button-ghost btnDisabled";
  } else {
    btnCadastro = "card-form-button button-ghost";
  }

  async function userLogin(e) {
    e.preventDefault();

    //Activate loading screen
    setLoading(true);

    const user = {
      email: document.getElementById("email").value,
      password: document.getElementById("senha").value,
    };

    //Desativar o botão de cadastro por algum tempo
    setCadastroDisabled(true);

    //consultar api
    await api
      .post("/user/auth/login", user)
      .then(result => {
        setOpenLogin(!openLogin);
        setUser(result.data.user);

        if (result.data.user.isAdmin) {
          setIsAdmin(true);
        }

        //Store token in localstorage
        localStorage.setItem("wolf-token", result.data.token);
        localStorage.setItem("wolf-username", result.data.user.name);
        localStorage.setItem(
          "wolf-user-data",
          JSON.stringify(result.data.user)
        );

        //Usuário logado
        setIsLoggedIn(true);
        //const pathname = window.location.pathname;
        //console.log(pathname);
        //history.push('/');
        //history.replace(pathname);
        history.go(0);
        enqueueSnackbar("Login bem sucedido", { variant: "success" });
      })
      .catch(error => {
        setIsLoggedIn(false);
        console.log(error.response);
        enqueueSnackbar("Erro: Falha na autenticação", {
          variant: "error",
        });
      })
      .finally(() => {
        clearNotification();

        //Ativar o botão de cadastro
        setCadastroDisabled(false);

        //Deactivate loading screen
        setLoading(false);
      });
  }

  function handleChange(e) {
    setUserDetails({
      ...userDetails,
      [e.target.name]: e.target.value,
    });

    const user = {
      email: document.getElementById("email").value,
      password: document.getElementById("senha").value,
    };

    if (user.email === "" || user.password === "") {
      setCadastroDisabled(true);
    } else {
      setCadastroDisabled(false);
    }
  }

  return (
    <>
      <div className="formUsuario">
        <div className="login-card">
          <h2>Fazer Login</h2>
          <div className="card-grid">
            <div className="col-50 card-cell card-login mob-view-100">
              <form autoComplete="off" className="card-form">
                <label>E-mail *</label>
                <input
                  autoComplete="disabled2"
                  type="text"
                  id="email"
                  name="email"
                  required
                  value={userDetails.email}
                  onChange={e => handleChange(e)}
                />
                <label>Senha *</label>
                <div className="password-input">
                  <input
                    autoComplete="disabled5"
                    type={visiblePassword ? "text" : "password"}
                    id="senha"
                    name="password"
                    required
                    value={userDetails.password}
                    onChange={e => handleChange(e)}
                  />
                  {visiblePassword ? (
                    <BsEyeSlash
                      className="passwordVisibility"
                      onClick={() => {
                        setVisiblePassword(!visiblePassword);
                      }}
                    />
                  ) : (
                    <BsEye
                      className="passwordVisibility"
                      onClick={() => {
                        setVisiblePassword(!visiblePassword);
                      }}
                    />
                  )}
                </div>
                <button
                  type="submit"
                  className={btnCadastro}
                  disabled={cadastrarDisabled}
                  onClick={e => userLogin(e)}>
                  Entrar
                </button>
              </form>
            </div>
          </div>
        </div>

        {/* <Footer  /> */}
      </div>
    </>
  );
}
