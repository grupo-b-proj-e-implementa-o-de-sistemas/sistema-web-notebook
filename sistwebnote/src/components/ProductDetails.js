import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import "../styles/productDetails.css";
import api from "../api/api";
import CompareOffers from "./CompareOffers";
import Reviews from "./Reviews";

export default function ProductDetails() {
  const [product, setProduct] = useState();
  const [loading, setLoading] = useState(true);
  const [loadingText, setLoadingText] = useState("Carregando produto...");

  const { id } = useParams();

  //Mantenha a tela no topo
  window.scrollTo(0, 0);

  //const { url } = useRouteMatch();

  useEffect(() => {
    api("/notebooks/" + id)
      .then(result => {
        setProduct(result.data);
        //console.log(result.data);
        setLoading(false);
      })
      .catch(error => {
        //console.log(error);
        setLoading(true);
        setLoadingText("Falha em carregar informação do produto");
      });
  }, [id]);

  if (loading) {
    return (
      <div className="produto-main">
        <div className="produto-container">{loadingText}</div>
      </div>
    );
  } else {
    return (
      <div className="product-details-container">
        <div className="product-details-info">
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}>
            <h1 className="product-details-title">{product.name}</h1>
            <Link to="/">Voltar</Link>
          </div>
          <div className="product-details-content">
            <div className="product-details-card">
              <img
                src={product.imgUrl}
                alt={product.name}
                className="product-details-image"
              />
            </div>
            <div className="product-details-description">
              <div className="product-details-description-item">
                <h4>Marca</h4>
                <p>{product.brand}</p>
              </div>
              <div className="product-details-description-item">
                <h4>Uso específico</h4>
                <p>{product.type}</p>
              </div>
              <div className="product-details-description-item">
                <h4>Tamanho da tela</h4>
                <p>{product.screenSize} Polegadas</p>
              </div>
              <div className="product-details-description-item">
                <h4>Processador</h4>
                <p>{product.processor}</p>
              </div>
              <div className="product-details-description-item">
                <h4>Cor</h4>
                <p>{product.color}</p>
              </div>
              <div className="product-details-description-item">
                <h4>Teclado</h4>
                <p>{product.keyboard}</p>
              </div>
            </div>
          </div>
        </div>
        <CompareOffers stores={product.stores} />
        <Reviews notebook_id={id} />
      </div>
    );
  }
}
