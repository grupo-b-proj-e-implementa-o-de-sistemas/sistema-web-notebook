import { useContext, useState } from "react";
import { Link, useRouteMatch } from "react-router-dom";
import { User } from "../Context/UserContext";
import defaultImg from "../images/default_profile_pic.png";
import ImageViewer from "./imageViewer";

export default function NavDropdown() {
  const [openImageViewer, setOpenImageViewer] = useState(false);
  //Context
  const { user, setIsAdmin, isAdmin } = useContext(User);
  const { url } = useRouteMatch();
  const [imgData, setImageData] = useState("");
  const image = user.image ?? defaultImg;

  if (openImageViewer) {
    document.documentElement.style.overflow = "hidden";
    document.body.scroll = "no"; //Internet Explorer
  } else {
    document.documentElement.style.overflow = "auto";
    document.body.scroll = "yes"; //Internet Explorer
  }

  function handleOpenImage(data) {
    setImageData(data);
    setOpenImageViewer(!openImageViewer);
  }

  function handleLogout() {
    localStorage.clear();
    window.location.reload(false);
    setIsAdmin(false);
  }

  return (
    <>
      <div className="dropdown-container">
        <div className="dropdown-div-1">
          <img
            src={image}
            alt="profile-pic"
            onClick={() => handleOpenImage(image)}
          />
        </div>
        <div className="dropdown-div-2">
          <button className="dropdownBtn">Olá, {user.name}</button>
          <div className="dropdown-content">
            <div className="dropdown-content-center">
              {isAdmin && <Link to={`${url}mensagens`}>Mensagens</Link>}
              <button onClick={() => handleLogout()}>Logout</button>
            </div>
          </div>
        </div>
      </div>
      {openImageViewer && (
        <ImageViewer
          openImageViewer={openImageViewer}
          setOpenImageViewer={setOpenImageViewer}
          image={imgData}
        />
      )}
    </>
  );
}
