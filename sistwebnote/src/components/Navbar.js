// import { FaReact } from "react-icons/fa";
import { AiOutlineMenu } from "react-icons/ai";
import { AiOutlineClose } from "react-icons/ai";
import { HiOutlineUser } from "react-icons/hi";
import { Link, useRouteMatch, NavLink } from "react-router-dom";
import { useState } from "react";
import RegisterForm from "./RegisterForm";
import Login from "./LoginForm";
import "../styles/navbar.css";
import Logo from "../images/logo.png";
import NavDropdown from "./NavDropdown";
import { useContext } from "react";
import { User } from "../Context/UserContext";

let login_class = "login-fechado";
let login_tela = "ClarearTela";

const Navbar = () => {
  const [openLogin, setOpenLogin] = useState(false);
  const [openCadastro, setOpenCadastro] = useState(false);
  const [openMenu, setOpenMenu] = useState(false);
  //Context
  const { isLoggedIn } = useContext(User);

  if (openLogin) {
    login_class = "login";
    login_tela = "EscurecerTela";
  } else {
    login_tela = "ClarearTela";
    login_class = "login-fechado";
  }

  function abrirLogin() {
    setOpenLogin(!openLogin);
    if (!openLogin) {
      setOpenCadastro(false);
      // window.onscroll = null;
      document.documentElement.style.overflow = "hidden";
      document.body.scroll = "no"; //Internet Explorer
    } else {
      document.documentElement.style.overflow = "auto";
      document.body.scroll = "yes"; //Internet Explorer
    }
  }

  function OpenNavMenu() {
    setOpenMenu(!openMenu);
  }

  const { url } = useRouteMatch();

  return (
    <header className="nav">
      <div className="navbar1">
        <div className="logo">
          {/* <FaReact size="2rem" color="#1e5bc6" /> */}
          <Link to={`${url}`}>
            <img src={Logo} alt="site logo" />
          </Link>
        </div>
        {/* Desktop View */}
        <div className="desktop-menu-view">
          <div className="nav-items">
            <ul>
              <li>
                <NavLink to={`${url}`} exact activeClassName="active-nav-item">
                  Inicial
                </NavLink>
              </li>
              <li>
                <NavLink to={`${url}lojas`} activeClassName="active-nav-item">
                  Lojas
                </NavLink>
              </li>
              <li>
                <NavLink to={`${url}regioes`} activeClassName="active-nav-item">
                  Regiões
                </NavLink>
              </li>
              <li>
                <NavLink to={`${url}contato`} activeClassName="active-nav-item">
                  Contato
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
        {/* End of Desktop View */}

        {/* Mobile Menu View */}
        {openMenu && (
          <div className="mobile-menu-view">
            <AiOutlineClose
              className="close-nav-btn"
              size="1.5rem"
              onClick={() => OpenNavMenu()}
            />
            <div className="nav-items">
              <ul>
                <li>
                  <NavLink
                    to={`${url}`}
                    exact
                    activeClassName="active-nav-item"
                    onClick={() => OpenNavMenu()}>
                    Inicial
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to={`${url}lojas`}
                    activeClassName="active-nav-item"
                    onClick={() => OpenNavMenu()}>
                    Lojas
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to={`${url}regioes`}
                    activeClassName="active-nav-item"
                    onClick={() => OpenNavMenu()}>
                    Regiões
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to={`${url}contato`}
                    activeClassName="active-nav-item"
                    onClick={() => OpenNavMenu()}>
                    Contato
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
        )}
        {/* End of Mobile menu view */}
        <div className="navDireita">
          {isLoggedIn ? (
            <div className="div-user">
              <NavDropdown />
            </div>
          ) : (
            <div className="div-user" onClick={() => abrirLogin()}>
              <HiOutlineUser className="user-icon" size="2rem" />
              <div className="bem-vindo">
                <label className="label-bv">Bem Vindo</label>
                <label> Entre</label>
              </div>
            </div>
          )}
          <AiOutlineMenu
            className="mobile-menu"
            size="1.5rem"
            onClick={() => OpenNavMenu()}
          />
          <div className={login_class}>
            {openLogin && (
              <>
                <AiOutlineClose
                  className="close-nav-btn"
                  size="1.5rem"
                  onClick={() => abrirLogin()}
                />
                {openCadastro ? (
                  <RegisterForm
                    openLogin={openLogin}
                    setOpenLogin={setOpenLogin}
                    openCadastro={openCadastro}
                    setOpenCadastro={setOpenCadastro}
                  />
                ) : (
                  <Login openLogin={openLogin} setOpenLogin={setOpenLogin} />
                )}
              </>
            )}
            {openCadastro ? (
              <div className="form-btn">
                <div className="form-btn-text">
                  <p>Já está cadastrado?</p>{" "}
                  <button
                    className="form-btn-btn"
                    onClick={() => setOpenCadastro(false)}>
                    Acesse aqui
                  </button>
                </div>
              </div>
            ) : (
              <div className="form-btn">
                <div className="form-btn-text">
                  <p>Não tem uma conta?</p>{" "}
                  <button
                    className="form-btn-btn"
                    onClick={() => setOpenCadastro(true)}>
                    Cadastre-se aqui!
                  </button>
                </div>
              </div>
            )}
          </div>
          <div className={login_tela} onClick={() => abrirLogin()}></div>
        </div>
      </div>
      <div className="linha-horizontal" />
      {/* <div className="navbar2"></div> */}
    </header>
  );
};

export default Navbar;
