import "../styles/loading.css";
import { ScaleLoader } from "react-spinners";
import { User } from "../Context/UserContext";
import { useContext } from "react";

export default function Loading() {
  const { loading } = useContext(User);
  if (loading) {
    document.documentElement.style.overflow = "hidden";
    document.body.scroll = "no"; //Internet Explorer

    return (
      <div className="spinner-container">
        <div className="spinner-inner-container">
          <ScaleLoader
            size={35}
            width={10}
            radius={2}
            color="#FFFFFF"
            margin={4}
          />
        </div>
      </div>
    );
  } else {
    document.documentElement.style.overflow = "auto";
    document.body.scroll = "yes"; //Internet Explorer
    return <></>;
  }
}
