import "../styles/imageViewer.css";
import { AiOutlineClose } from "react-icons/ai";

export default function ImageViewer({ image, setOpenImageViewer }) {
  return (
    <div className="image-main">
      <div className="image-container">
        <div className="image-header">
          <AiOutlineClose
            onClick={() => setOpenImageViewer(false)}
            className="close-image-btn"
            size="1.5rem"
          />
        </div>
        <div className="image-content">
          <img src={image} alt="show data" />
        </div>
      </div>
    </div>
  );
}
