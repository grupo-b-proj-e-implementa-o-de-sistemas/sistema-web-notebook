import '../styles/compareOffers.css';

const CompareOffers = ({ stores }) => {

    const formatPrice = (price) => {
        return new Intl.NumberFormat('pt-BR', { minimumFractionDigits: 2 }).format(price);
    }

    return (
        <div className="compareOffersContainer">
            <div className="bar">
                <h4>Compare</h4>
            </div>
            {Boolean(stores.length) ?
                <div className="compareOffersContent">
                    {
                        stores.map(store => (
                            <a
                                className="offerItem" key={store.offer._id}
                                href={store.offer.url}
                                target="_blank" rel="noreferrer">
                                <div className="offerItemImgContent">
                                    <img
                                        src={`../images/logo_lojas/${store.logo}.png`}
                                        alt={store.name}
                                    />
                                </div>
                                <div className="priceContent">
                                    <h4>R$ {formatPrice(store.offer.price)}</h4>
                                </div>
                            </a>
                        ))
                    }
                </div>
                    : <p style={{ textAlign: 'center', margin: 15 }} >Não há ofertas para este produto</p>
                }
        </div>
    )
};

export default CompareOffers;