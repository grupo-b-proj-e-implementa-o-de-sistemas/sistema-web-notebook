import { BsSearch } from "react-icons/bs";
import '../styles/filter.css';

const Filter = ({ onChangeName }) => {
    return (
        <div className="filterContainer">
            <div id="div-search" className="div-search">
                <input
                type="text"
                className="search"
                placeholder="O que deseja procurar?"
                onChange={item => onChangeName(item.target.value)}
                />
          </div>
          <div className="lupa">
            <BsSearch className="lupa-icon" size="1.5rem" color="#fff" />
          </div>
        </div>
    );
};

export default Filter;