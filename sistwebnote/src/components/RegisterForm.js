import { useState } from "react";
import "../styles/loginForm.css";
import { BsUpload } from "react-icons/bs";
//Checagem da senha
import { passwordStrength } from "check-password-strength";
import { defaultOptions } from "../utils/checkPassword";
//Pacote para notificações no sistema
import { useSnackbar } from "notistack";
import api from "../api/api";
import { BsEye, BsEyeSlash } from "react-icons/bs";
import { useContext } from "react";
import { User } from "../Context/UserContext";

//Class do botão de cadastro
let btnCadastro = "card-form-button button-ghost";
//Class da senha input
let inputPassword = "cadastro-senha";

export default function RegisterForm({
  openLogin,
  setOpenLogin,
  openCadastro,
  setOpenCadastro,
}) {
  const [cadastrarDisabled, setCadastroDisabled] = useState(true);
  const [imagemNome, setImagemNome] = useState(); //Vetor de estados
  const [imagemCarregada, setImagemCarregada] = useState(false); //Vetor de estados
  const [imagemArquivo, setImagemArquivo] = useState(""); //Vetor de estados
  //Variavel utilizado para checagem
  const [userDetails, setUserDetails] = useState({
    name: "",
    email: "",
    city: "",
    country: "",
    password: "",
    image: "",
  });
  //Revelar a senha
  const [visiblePassword, setVisiblePassword] = useState(false);
  const [register, setRegister] = useState(false);

  //Context
  const { setLoading, clearNotification } = useContext(User);

  //Notificação
  const { enqueueSnackbar } = useSnackbar();

  if (cadastrarDisabled) {
    btnCadastro = "card-form-button button-ghost btnDisabled";
  } else {
    btnCadastro = "card-form-button button-ghost";
  }

  //check password and change input field color based on the strength
  function checkPassword() {
    let passwordValue = document.getElementById("senha").value;

    if (passwordValue !== "") {
      const strengthPassword = passwordStrength(
        passwordValue,
        defaultOptions
      ).value;

      if (strengthPassword === "Too weak" || strengthPassword === "Weak") {
        inputPassword = "cadastro-senha senha-fraca";
      } else {
        inputPassword = "cadastro-senha senha-aceita";
        setRegister(true);
        return;
      }
    } else {
      inputPassword = "cadastro-senha";
    }

    setRegister(false);
  }

  async function Cadastrar(e) {
    e.preventDefault();

    //Activate loading screen
    setLoading(true);

    const user = {
      name: document.getElementById("nome").value,
      email: document.getElementById("email").value,
      city: document.getElementById("cidade").value,
      country: document.getElementById("pais").value,
      password: document.getElementById("senha").value,
      image: imagemArquivo,
    };

    //Desativar o botão de cadastro por algum tempo
    setCadastroDisabled(true);

    //consultar api
    await api
      .post("/users", user)
      .then(() => {
        enqueueSnackbar("Cadastro realizado", { variant: "success" });
        setOpenLogin(!openLogin);
        inputPassword = "";

        //Desativar a página de cadastro
        setOpenCadastro(!openCadastro);
      })
      .catch(error => {
        console.log(error.response);
        enqueueSnackbar("Erro: " + error.response.data.message, {
          variant: "error",
        });
      })
      .finally(() => {
        clearNotification();
        //Ativar o botão de cadastro
        setCadastroDisabled(false);

        //Deactivate loading screen
        setLoading(false);
      });
  }

  function handleChange(e) {
    setUserDetails({
      ...userDetails,
      [e.target.name]: e.target.value,
    });

    checkPassword();

    const user = {
      name: document.getElementById("nome").value,
      email: document.getElementById("email").value,
      city: document.getElementById("cidade").value,
      country: document.getElementById("pais").value,
      password: document.getElementById("senha").value,
      image: imagemArquivo,
    };
    if (
      user.name === "" ||
      user.email === "" ||
      user.city === "" ||
      user.country === "" ||
      user.password === ""
    ) {
      setCadastroDisabled(true);
    } else {
      if (register) {
        setCadastroDisabled(false);
      } else {
        setCadastroDisabled(true);
      }
    }
  }

  const onChangeImageHandler = async e => {
    const selectedImage = e.target.files[0];
    //console.log(selectedImage);

    const data = new FormData();
    data.append("image", selectedImage);
    // console.log(data);

    await api
      .post("/users/upload", data)
      .then(res => {
        //console.log(res.data.uri);
        setImagemArquivo(res.data.uri);
        setImagemNome(res.data.uri);
        setImagemCarregada(true);
      })
      .catch(err => {
        console.log(err);
        enqueueSnackbar("Falha no upload da Imagem!", { variant: "error" });
        clearNotification();
      });
  };

  return (
    <>
      <div className="formUsuario">
        <div className="login-card">
          <h2>Cadastrar Usuário</h2>
          <div className="card-grid">
            <div className="col-50 card-cell card-login">
              <form autoComplete="off" className="card-form">
                <label>Nome *</label>
                <input
                  autoComplete="disabled1"
                  type="text"
                  id="nome"
                  name="name"
                  required
                  autoFocus
                  value={userDetails.name}
                  onChange={e => handleChange(e)}
                />
                <label>E-mail *</label>
                <input
                  autoComplete="disabled2"
                  type="text"
                  id="email"
                  name="email"
                  required
                  value={userDetails.email}
                  onChange={e => handleChange(e)}
                />
                <label>Cidade *</label>
                <input
                  autoComplete="disabled3"
                  type="text"
                  id="cidade"
                  name="city"
                  required
                  value={userDetails.city}
                  onChange={e => handleChange(e)}
                />
                <label>País *</label>
                <input
                  autoComplete="disabled4"
                  type="text"
                  id="pais"
                  name="country"
                  required
                  value={userDetails.country}
                  onChange={e => handleChange(e)}
                />
                <label className="form-senha-input">Senha *</label>
                <div className={inputPassword}>
                  <input
                    autoComplete="disabled5"
                    type={visiblePassword ? "text" : "password"}
                    id="senha"
                    name="password"
                    required
                    value={userDetails.password}
                    onChange={e => handleChange(e)}
                  />
                  {visiblePassword ? (
                    <BsEyeSlash
                      className="passwordVisibility"
                      onClick={() => {
                        setVisiblePassword(!visiblePassword);
                      }}
                    />
                  ) : (
                    <BsEye
                      className="passwordVisibility"
                      onClick={() => {
                        setVisiblePassword(!visiblePassword);
                      }}
                    />
                  )}
                </div>
                <button
                  type="submit"
                  className={btnCadastro}
                  disabled={cadastrarDisabled}
                  onClick={e => Cadastrar(e)}>
                  Cadastrar
                </button>
              </form>
            </div>
            <div className={!imagemCarregada ? "load-img" : "loaded-img"}>
              {!imagemCarregada && <BsUpload size="4rem" />}
              {!imagemCarregada && "Adicionar Imagem"}

              {/* <p>{imagemNome}</p> */}
              <input
                type="file"
                className="inputImagem"
                name="image"
                onChange={e => onChangeImageHandler(e)}
              />
              {imagemCarregada && (
                <img
                  src={imagemNome}
                  className="imgPreview"
                  alt="imagem-perfil"
                />
              )}
            </div>
          </div>
        </div>

        {/* <Footer /> */}
      </div>
    </>
  );
}
