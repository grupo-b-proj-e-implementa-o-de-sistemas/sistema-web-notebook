import CookieConsent from "react-cookie-consent";

export default function CookieNotification() {
  return (
    <CookieConsent
      location="bottom"
      buttonText="Fechar e continuar"
      cookieName="wolves-aviso"
      style={{ background: "#2B373B" }}
      buttonStyle={{
        backgroundColor: "var(--color-default)",
        color: "white",
        fontSize: "13px",
        borderRadius: "10px",
      }}>
      <strong>wolves notebooks:</strong> nós usamos cookies para armazenar dados
      do site e melhorar a sua experiência. Ao continuar navegando, você
      concorda com esse termo.
    </CookieConsent>
  );
}
