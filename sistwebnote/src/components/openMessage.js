import "../styles/Messages.css";
import { AiOutlineClose } from "react-icons/ai";

export default function OpenMessage({ data, setOpenModal }) {
  return (
    <div className="message-details-main">
      <div className="message-details-container">
        <div className="message-details-header">
          <h4>De: {data.email}</h4>
          <AiOutlineClose
            onClick={() => setOpenModal(false)}
            className="close-message-btn"
            size="1.5rem"
          />
        </div>
        <div className="message-details-content">
          <h5>{data.text}</h5>
        </div>
      </div>
    </div>
  );
}
