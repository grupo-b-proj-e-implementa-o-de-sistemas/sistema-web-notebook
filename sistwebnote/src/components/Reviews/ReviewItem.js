import ReactStars from "react-rating-stars-component";
import defaultImg from "../../images/default_profile_pic.png";


const scoreMap = {
    VERY_BAD: 1,
    BAD: 2,
    MEDIUM: 3,
    GOOD: 4,
    GREAT: 5
}

const ReviewItem = ({ review }) => {
    //const defaultImg = "https://upload.wikimedia.org/wikipedia/commons/6/60/OpenEuphoria_mascot_200px.png";

    return (
        <div className="review-item-content">
            {/* {console.log(review)} */}
            <img
                src={review.userImg ?? defaultImg}
                alt={review.username} width={95} height={88}
                data-testid="user-image"
            />
            <h4>{review.username}</h4>
            <div className="review-item-score-text">
                <ReactStars size={30} value={scoreMap[review.score]} edit={false} color={"white"} data-testid="user-score" />
                <p>{review.text}</p>
            </div>
        </div>
    )
};

export default ReviewItem;