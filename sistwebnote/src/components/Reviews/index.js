import { useCallback, useEffect, useContext } from "react";
import { User } from "../../Context/UserContext";
import { useState } from "react";
import api from "../../api/api";
import ReviewItem from "./ReviewItem";
import "../../styles/reviews.css";
import CreateReview from "./CreateReview";
import { useSnackbar } from "notistack";
import { useHistory } from "react-router-dom";

const Reviews = ({ notebook_id }) => {
  const { clearNotification } = useContext(User);
  const [reviews, setReviews] = useState();
  const token = localStorage.getItem("wolf-token") ?? "";
  const [reviewData, setReviewData] = useState({
    notebook_id,
    score: "VERY_BAD",
    text: "",
  });
  //const [isLogged, setIsLogged] = useState(false);
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();

  const getReviews = useCallback(() => {
    //setIsLogged(Boolean(token));
    api(`/reviews/findByNotebook/${notebook_id}?token=${token}`)
      .then(res => {
        setReviews(res.data);
        //console.log(Boolean(token));
        //console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }, [notebook_id, token]);

  const onSubmitReview = e => {
    e.preventDefault();
    //console.log(reviewData);
    if (reviewData.text === "") {
      enqueueSnackbar("Por favor, escreva algo sobre o produto", {
        variant: "error",
      });
      clearNotification();
      return;
    } else {
      api
        .post(`/reviews/${token}`, reviewData)
        .then(res => {
          //console.log(res);
          history.push("/");
          history.replace(`/produto-especifico/id=${notebook_id}`);
          enqueueSnackbar("Avaliação realizada com sucesso!", {
            variant: "success",
          });
        })
        .catch(err => {
          console.log(err);
          enqueueSnackbar("Erro ao criar Avaliação!", { variant: "error" });
        })
        .finally(() => {
          clearNotification();
        });
    }
  };

  useEffect(() => {
    getReviews();
  }, [getReviews]);

  return (
    <div className="reviewsContainer">
      <div className="bar">
        <h4>Avaliações</h4>
      </div>
      <div className="reviewsContent">
        {reviews && Boolean(reviews.length) ? (
          reviews.map(review =>
            review ? (
              <ReviewItem review={review} key={review._id} />
            ) : (
              <CreateReview
                key={0}
                data={reviewData}
                setData={setReviewData}
                onSubmit={onSubmitReview}
              />
            )
          )
        ) : (
          <p style={{ textAlign: "center" }}>
            Este produto ainda não possui Avaliação
          </p>
        )}
      </div>
    </div>
  );
};

export default Reviews;
