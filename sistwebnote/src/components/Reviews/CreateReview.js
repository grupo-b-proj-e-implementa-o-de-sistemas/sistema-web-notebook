import ReactStars from "react-rating-stars-component";
import { useContext } from "react";
import { User } from "../../Context/UserContext";
import defaultImage from "../../images/default_profile_pic.png";
const scoreMap = {
  VERY_BAD: 1,
  BAD: 2,
  MEDIUM: 3,
  GOOD: 4,
  GREAT: 5,
};

const getScoreKey = value => {
  return Object.keys(scoreMap).find(key => scoreMap[key] === value);
};

const CreateReview = ({ data, setData, onSubmit }) => {
  const { user } = useContext(User);

  return (
    <div className="create-review-container">
      <img
        src={user.image ?? defaultImage}
        alt={user.name}
        width={95}
        height={88}
      />
      <form className="create-review-form" onSubmit={onSubmit}>
        <h4>{user.name}</h4>
        <ReactStars
          size={30}
          value={scoreMap[data.score]}
          onChange={item => setData({ ...data, score: getScoreKey(item) })}
        />
        <textarea
          className="create-review-form-text"
          placeholder="Diga algo sobre o produto..."
          onChange={item => setData({ ...data, text: item.target.value })}
        />
        <button className="create-review-form-btn">
          <h4>Avaliar</h4>
        </button>
      </form>
    </div>
  );
};

export default CreateReview;
