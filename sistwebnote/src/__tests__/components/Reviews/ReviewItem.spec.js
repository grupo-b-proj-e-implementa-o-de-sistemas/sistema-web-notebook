import { render, screen } from "@testing-library/react";
import ReviewItem from "../../../components/Reviews/ReviewItem";

test('should render review item when', () => {
    const review = {
        username: "devnote",
        userImg: "devnote.png",
        score: "MEDIUM",
        text: "some text"
    };

    render(
        <ReviewItem review={review} />
    );

    const usernameElement = screen.getByText(review.username);
    const userImgElement = screen.getByTestId("user-image");
    const textElement = screen.getByText(review.text);
    const scoreElement = screen.getByText("3");

    expect(usernameElement).toBeInTheDocument();
    expect(userImgElement).toBeInTheDocument();
    expect(textElement).toBeInTheDocument();
    expect(scoreElement).toBeInTheDocument();

    //screen.debug();
});