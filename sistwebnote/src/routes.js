import React, { Suspense, lazy } from "react";
import ErrorBoundary from "./utils/ErrorBoundary";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Loading from "./components/Loading";
import UserContext from "./Context/UserContext";
import RouterProvider from "./Context/RouterContext";

const Home = lazy(() => import("./pages/Home"));
const Page404 = lazy(() => import("./pages/Page404"));

export default function Routes() {
  return (
    <BrowserRouter>
      <ErrorBoundary>
        <Suspense fallback={<div className="lazy-loading">Carregando...</div>}>
          <UserContext>
            <Loading />
            <RouterProvider>
              <Switch>
                <Route path="/" component={Home} />
                <Route path="*" component={Page404} />
                {/* <Route path="/products" exact component={Pagina2} /> */}
              </Switch>
            </RouterProvider>
          </UserContext>
        </Suspense>
      </ErrorBoundary>
    </BrowserRouter>
  );
}
