import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import ProductListPage from "./ProductListPage";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useRouteMatch,
} from "react-router-dom";
import ContactPage from "./ContactPage";
import Produto from "./Produto";
import Page404 from "./Page404";
import RegiaoPage from "./RegiaoPage";
import LojasPage from "./LojasPage";
import ProductDetails from "../components/ProductDetails";
import CookieNotification from "../components/CookieNotification";
import Messages from "./Messages";

export default function Home() {
  const { path } = useRouteMatch();
  return (
    <Router>
      <div className="home-container">
        <Navbar />

        <Switch>
          <Route path="/" exact component={ProductListPage} />
          <Route path={`${path}contato`} component={ContactPage} />
          <Route path={`${path}produto`} component={Produto} />
          <Route
            path={`${path}produto-especifico/id=:id`}
            component={ProductDetails}
          />
          <Route path={`${path}regioes`} component={RegiaoPage} />
          <Route path={`${path}lojas`} component={LojasPage} />
          <Route path={`${path}mensagens`} component={Messages} />
          <Route component={Page404} />
        </Switch>
        <CookieNotification />
        <Footer />
      </div>
    </Router>
  );
}
