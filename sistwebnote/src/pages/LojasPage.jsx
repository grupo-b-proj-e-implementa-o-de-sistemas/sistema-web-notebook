import { useState, useEffect } from "react";
import "../styles/Loja.css";
import api from "../api/api";

export default function LojasPage() {
  const [stores, setStores] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingText, setLoadingText] = useState("Carregando...");

  //Mantenha a tela no topo
  window.scrollTo(0, 0);

  useEffect(() => {
    setLoadingText("Buscando lojas...");
    api
      .get("/stores")
      .then(result => {
        setStores(result.data);
        setLoading(false);
      })
      .catch(error => {
        console.log(error);
        setLoadingText("Falha na busca de lojas. Por favor, tente mais tarde");
        setLoading(true);
      });
  }, [stores.length]);

  if (loading) {
    return (
      <div className="loja-main">
        <div className="loja-container">{loadingText}</div>
      </div>
    );
  } else {
    return (
      <div className="loja-main-page">
        <div className="loja-container-main">
          {stores.map(store => {
            return (
              <div className="loja-container-card" key={store._id}>
                <a href={store.url} target="_blank" rel="noreferrer">
                  <div className="loja-image">
                    <img
                      src={`../images/logo_lojas/${store.logo}.png`}
                      alt={store.name}
                    />
                  </div>
                  <hr />
                  <div className="loja-name">
                    <h5>{store.name}</h5>
                  </div>
                </a>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
