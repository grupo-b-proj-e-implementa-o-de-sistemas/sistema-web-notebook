import "../styles/Regiao.css";
//import { useState, useEffect } from "react";
//https://github.com/felipefdl/cidades-estados-brasil-json/blob/master/Estados.json
import Region from "../api/regiao.json";

export default function RegiaoPage() {
  //Mantenha a tela no topo
  window.scrollTo(0, 0);

  return (
    <div className="regiao-main-page">
      <div className="regiao-container-main">
        {Region.map(region => {
          return (
            <div className="regiao-container-card" key={region.id}>
              <div className="regiao-name">
                <h5>
                  {region.Sigla} - {region.Nome}
                </h5>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
