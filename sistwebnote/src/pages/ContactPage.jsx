import { useState, useContext } from "react";
import "../styles/contactPage.css";
import { FaEnvelope, FaMapMarkerAlt, FaPhoneAlt } from "react-icons/fa";
import { User } from "../Context/UserContext";
import { useSnackbar } from "notistack";
import api from "../api/api";

export default function ContactPage() {
  const [userMessage, setUserMessage] = useState("");
  const [email, setEmail] = useState("");
  const { enqueueSnackbar } = useSnackbar();

  const { setLoading, clearNotification } = useContext(User);

  function handleSubmit() {
    if (
      email === "" ||
      !email.includes("@") ||
      email.length < 3 ||
      userMessage === "" ||
      userMessage.length < 6
    ) {
      return;
    } else {
      setLoading(true);
      const data = {
        email: email,
        text: userMessage,
      };
      api
        .post("/messages", data)
        .then(() => {
          enqueueSnackbar("Mensagem foi enviado", { variant: "success" });
          setEmail("");
          setUserMessage("");
        })
        .catch(error => {
          console.log(error);
          enqueueSnackbar("Mensagem não foi enviado", { variant: "error" });
        });
    }
    setLoading(false);
    clearNotification();
  }

  return (
    <div className="contact-main">
      <div className="contact-container">
        <div className="contact-content">
          <div className="left-side">
            <div className="address details">
              <FaMapMarkerAlt className="icon" />
              <div className="topic">Endereço</div>
              <div className="text-one">Boa Vista</div>
              <div className="text-two">Roraima, Brasil</div>
            </div>
            <div className="phone details">
              <FaPhoneAlt className="icon" />
              <div className="topic">Telefone</div>
              <div className="text-one">+55 (95) 9999 9999</div>
              <div className="text-two">+55 (95) 9999 9998</div>
            </div>
            <div className="email details">
              <FaEnvelope className="icon" />
              <div className="topic">Email</div>
              <div className="text-one">contato@wolves.com</div>
              <div className="text-two">info.email@wolves.com</div>
            </div>
          </div>
          <div className="right-side">
            <div className="topic-text">Entre em contato conosco</div>
            <p>Em caso de sugestões ou reclamações, nos envie uma mensagem</p>
            <form action="#">
              <div className="input-box">
                <input
                  type="email"
                  placeholder="Digite seu email"
                  value={email}
                  onChange={e => setEmail(e.target.value)}
                />
              </div>
              <div className="input-box">
                <textarea
                  type="text"
                  placeholder="Digite sua mensagem"
                  value={userMessage}
                  onChange={e => setUserMessage(e.target.value)}
                />
              </div>
              <div className="input-box contact-message-box"></div>
              <div className="button">
                <input
                  type="button"
                  value="Enviar"
                  onClick={() => handleSubmit()}
                />
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

/**
 * Baseado no código https://codepen.io/fadzrinmadu/pen/QWpBKYr
 */
