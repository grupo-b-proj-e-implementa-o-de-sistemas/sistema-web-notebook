import { useState, useEffect } from "react";
import "../styles/Messages.css";
import { useLocation, Redirect } from "react-router-dom";
import { useContext } from "react";
import { User } from "../Context/UserContext";
import OpenMessage from "../components/openMessage";
import api from "../api/api";
import { useSnackbar } from "notistack";

export default function Messages() {
  let location = useLocation();
  const { isAdmin, clearNotification } = useContext(User);
  const [messages, setMessages] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingText, setLoadingText] = useState("Carregando mensagens");
  const [openModal, setOpenModal] = useState(false);
  const [messageDetails, setMessageDetails] = useState([]);
  const { enqueueSnackbar } = useSnackbar();
  // const [selectAll, setSelectAll] = useState(true);

  const [selectedMessages, setSelectedMessages] = useState([]);

  function deleteMessages(e) {
    e.preventDefault();
    console.log(selectedMessages);

    api
      .delete("messages/many/" + JSON.stringify(selectedMessages))
      .then(() => {
        enqueueSnackbar("Mensagem excluída", { variant: "success" });
        getMessages();
        setSelectedMessages([]);
      })
      .catch(error => {
        enqueueSnackbar("Erro em excluir mensagem", { variant: "error" });
        console.log(error);
      })
      .finally(() => clearNotification());
  }

  function handleMessageClick(data) {
    setMessageDetails(data);
    // Mark message as read
    if (!data.isRead) {
      api
        .post("/messages/markasread/" + data._id)
        .then(() => {
          console.log("Message marked as read");
        })
        .catch(error => {
          console.log(error);
        });
    }
    setOpenModal(true);
  }

  if (openModal) {
    // window.onscroll = null;
    document.documentElement.style.overflow = "hidden";
    document.body.scroll = "no"; //Internet Explorer
  } else {
    document.documentElement.style.overflow = "auto";
    document.body.scroll = "yes"; //Internet Explorer
  }

  //Select or deselect all messages function
  /*
  function selectMessages() {
    setSelectAll(!selectAll);
    var chk = document.getElementsByName("message");

    for (var i = 0; i < chk.length; i++) {
      if (chk[i].type === "checkbox") {
        if (selectAll) {
          chk[i].checked = true;
        } else {
          chk[i].checked = false;
        }
      }
    }
  }
  */

  async function getMessages() {
    await api
      .get("messages")
      .then(result => {
        setMessages(result.data);
        setLoading(false);

        if (result.data.length === 0) {
          setLoadingText("Sua caixa de mensagens está vazia");
        }
      })
      .catch(error => {
        console.log(error);
        setLoading(true);
        setLoadingText("Falha em carregar as mensagens. Tente mais tarde.");
      });
  }

  useEffect(() => {
    getMessages();
  }, [messages.length]);

  if (isAdmin) {
    return (
      <>
        <div className="messages-main">
          <div className="messages-main-container">
            <div className="messages-header">
              <div className="messages-header-row">
                <div className="messages-header-first-part">
                  {selectedMessages.length !== 0 && (
                    <input
                      type="checkbox"
                      className="message-checkbox"
                      // onClick={() => {
                      //   selectMessages();
                      // }}
                    />
                  )}
                  <h4>Caixa de Mensagens ({messages.length})</h4>
                </div>
                {selectedMessages.length !== 0 && (
                  <button
                    className="btn-message-delete"
                    onClick={e => deleteMessages(e)}>
                    Excluir
                  </button>
                )}
              </div>
              <hr />
            </div>
            {loading ? (
              <div className="messages-content-loading">{loadingText}</div>
            ) : (
              <>
                <div className="messages-content">
                  {messages
                    .slice(0)
                    .reverse()
                    .map(message => {
                      return (
                        <>
                          <div className="message-box" key={message._id}>
                            <input
                              type="checkbox"
                              className="message-checkbox"
                              name="message"
                              value={selectedMessages}
                              onChange={e => {
                                // add to list
                                if (e.target.checked) {
                                  setSelectedMessages([
                                    ...selectedMessages,
                                    {
                                      id: message._id,
                                    },
                                  ]);
                                } else {
                                  // remove from list
                                  setSelectedMessages(
                                    selectedMessages.filter(
                                      item => item.id !== message._id
                                    )
                                  );
                                }
                              }}
                            />
                            <div
                              className="message-box-content"
                              onClick={() => handleMessageClick(message)}>
                              {message.isRead ? (
                                <h5 className="message-read">{message.text}</h5>
                              ) : (
                                <h5 className="message-not-read">
                                  {message.text}
                                </h5>
                              )}
                              <p>{message.email}</p>
                            </div>
                          </div>
                        </>
                      );
                    })}
                </div>
                <div className="messages-bottom">
                  Wolves Mail® - {new Date().getFullYear()}
                </div>
              </>
            )}
          </div>
        </div>
        {openModal && (
          <OpenMessage data={messageDetails} setOpenModal={setOpenModal} />
        )}
      </>
    );
  } else {
    return (
      <Redirect to={{ pathname: "/", state: { from: location.pathname } }} />
    );
  }
}
