import { useEffect, useState } from "react";
import "../styles/productList.css";
// import Loading from "./Loading";
import { useRouteMatch, Link } from "react-router-dom";
import api from "../api/api";
import Pagination from "../components/Pagination";
import Filter from "../components/Filter";

const ProductListPage = () => {
  const [products, setProducts] = useState([]);
  const [loadingProducts, setLoadingProducts] = useState(true);
  const [loadingText, setLoadingText] = useState("Carregando Produtos...");
  const { url } = useRouteMatch();
  const [activePage, setActivePage] = useState(0);
  const [productName, setProductName] = useState("");

  useEffect(() => {
    api(`/notebooks?page=${activePage}&name=${productName}`)
      .then(result => {
        setProducts(result.data);
        setLoadingProducts(false);
      })
      .catch(error => {
        console.log(error);
        setLoadingProducts(true);
        setLoadingText("Falha na busca de lojas. Por favor, tente mais tarde");
      });
  }, [activePage, productName]);

  if (loadingProducts) {
    return (
      <div className="produto-main">
        <div className="produto-container">{loadingText}</div>
      </div>
    );
  } else {
    return (
      <div className="totalList">
        <Filter onChangeName={name => {
          setProductName(name);
          //setActivePage(0);
        }} />
        <div className="container">
          <ul className="grid">
            {products.content.map(produto => {
              return (
                <li className="produto-card" key={produto._id}>
                  <Link
                    className="card__link"
                    to={`${url}produto-especifico/id=${produto._id}`}>
                    <img
                      className="card__image"
                      src={produto.imgUrl}
                      alt={produto.name}
                    />
                    <div className="card__text">{produto.name}</div>
                    <div className="card__price"></div>
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>
        <Pagination
          totalPages={products.totalPages}
          onChange={page => setActivePage(page)}
        />
      </div>
    );
  }
};

export default ProductListPage;
