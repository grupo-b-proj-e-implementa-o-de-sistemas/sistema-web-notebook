import React from "react";
import { Link } from "react-router-dom";
import "../styles/Page404.css";
import { useLocation } from "react-router-dom";

export default function Page404() {
  let location = useLocation();

  return (
    <div className="not-found-main">
      <div className="not-found-container">
        <p className="not-found-text">404</p>
        <p className="not-found-description">
          Página <strong>{location.pathname} não encontrada</strong>
        </p>
        <button className="not-found-button">
          Volte para a <Link to="/">Página inicial</Link>
        </button>
      </div>
    </div>
  );
}
