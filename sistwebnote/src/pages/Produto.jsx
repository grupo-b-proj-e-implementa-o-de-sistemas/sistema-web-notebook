import ProductDetails from "../components/ProductDetails";

export default function Produto() {
  return (
    <div className="produto-container">
      <ProductDetails />
    </div>
  );
}
