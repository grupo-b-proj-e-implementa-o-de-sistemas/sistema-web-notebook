import React from "react";
import ReactDOM from "react-dom";
import "./styles/index.css";
import reportWebVitals from "./reportWebVitals";
//Esse pacote é para utilizar como notificação no sistema
import { SnackbarProvider } from "notistack";
import Routes from "./routes";

// axios.defaults.baseURL = "https://wolves-backend.herokuapp.com/api/";

ReactDOM.render(
  <React.StrictMode>
    <SnackbarProvider
      maxSnack={1}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}>
      <Routes />
    </SnackbarProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint.
//Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
