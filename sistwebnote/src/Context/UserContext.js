import { useState, useEffect, createContext } from "react";
import { useSnackbar } from "notistack";

export const User = createContext();

const UserContext = ({ children }) => {
  const [user, setUser] = useState(
    JSON.parse(localStorage.getItem("wolf-user-data"))
  );
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [loading, setLoading] = useState(false);
  const [token, setToken] = useState("");
  const { closeSnackbar } = useSnackbar();

  //function to clear notification used globally
  function clearNotification() {
    //Clear notification after 2 seconds
    const timeoutId = setTimeout(() => {
      closeSnackbar();
    }, 2000);
    return () => clearTimeout(timeoutId);
  }

  useEffect(() => {
    setToken(localStorage.getItem("wolf-token"));

    if (token) {
      setIsLoggedIn(true);
      setIsAdmin(user.isAdmin);
    }

    // eslint-disable-next-line
  }, [token]);
  return (
    <User.Provider
      value={{
        user,
        setUser,
        isAdmin,
        setIsAdmin,
        loading,
        setLoading,
        isLoggedIn,
        setIsLoggedIn,
        clearNotification,
      }}>
      {children}
    </User.Provider>
  );
};

export default UserContext;
