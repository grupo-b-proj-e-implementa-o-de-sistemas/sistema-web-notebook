import { useState, useEffect, createContext } from "react";
import { useLocation } from "react-router-dom";

export const RouterContext = createContext();

const RouterProvider = ({ children }) => {
  const location = useLocation();
  const [route, setRoute] = useState({
    to: location.pathname,
    from: location.pathname, //--> previous pathname
  });

  /* To Do*/
  //   Implement RouterContext in the pages

  useEffect(() => {
    setRoute(prev => ({ to: location.pathname, from: prev.to }));
  }, [location]);

  return (
    <RouterContext.Provider value={{ route, setRoute }}>
      {children}
    </RouterContext.Provider>
  );
};

export default RouterProvider;
