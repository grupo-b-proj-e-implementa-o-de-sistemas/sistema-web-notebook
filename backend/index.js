const swaggerUi = require('swagger-ui-express');
const swaggerFile = require('./swagger_output.json');
const express = require("express");
const app = express();
const cors = require("cors");

const requireDir = require("require-dir");
// const routes = require("./src/routes");

const mongoose = require("mongoose");

const result = require("dotenv").config({silent: true});const endpointsFiles = ['./src/endpoints.js']
//mongoose.set('debug', true);

//Connection with MongoDB
mongoose.connect(process.env.DATABASE_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});
//Test the connection to the database

const db = mongoose.connection;
db.on("error", function (error) {
  console.log(error);
  //console.log("Trying to reconnect in 5 seconds...");
  //setTimeout( db, 5000);
});
db.once("open", function (callback) {
  console.log("Connection Successful!");
});

//MIDDLEWARES
// app.use(express.static("public"));
app.use(express.json());
app.use(cors());
requireDir("./src/models");
app.use("/api", require("./src/routes"));
app.use('/api/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile));
app.get('/', function(req, res) {
  res.redirect('/api/doc');
})

//define your port number here
const PORT = process.env.PORT ?? 5000;
app.listen(PORT, function () {
 console.log("Now listening for request on port: " + PORT);
});
