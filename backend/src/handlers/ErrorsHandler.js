const errorDict = {
    NotFound:{
        name: 'NotFound',
        error: 'Resource not found',
        status: 404
    },
    Conflict:{
        name: 'Conflict',
        error: 'Conflict',
        status: 409
    },
    UnprocessableEntity:{
        name: 'UnprocessableEntity',
        error: 'Validation failed',
        status: 422
    },
    Unauthorized:{
        name: 'Unauthorized',
        error: 'Unauthorized',
        status: 401
    },
    DatabaseIntegrity:{
        name: 'DatabaseIntegrity',
        error: 'Integrity violation',
        status: 400
    },
    BadRequest:{
        name: 'BadRequest',
        error: 'Bad request',
        status: 400
    },
};

const errorHandler = (errName, msg) => {

    const name = errorDict[errName].name;
    const error = errorDict[errName].error;
    const status = errorDict[errName].status;

    return (
        {
            name: name,
            status: status,
            body: {
                error: error,
                message: msg
            }
        }
    )
};

exports.errorHandler = (errName, msg) => errorHandler(errName, msg);

exports.errorService = (err) => {
    const error = (err.name in errorDict)
            ? err : errorHandler('BadRequest', err.message);
    return error;
};

// exports.errorResponse = (err, res) => {
//     const error = (err.name in errorDict)
//             ? err : errorHandler('BadRequest', err.message);
//     res.status(error.status).send(error.body);
// };