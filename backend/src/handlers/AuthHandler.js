const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const UserModel = mongoose.model("Users");
//const UserModel = require('../models/User')

//const SCOPES = ['https://www.googleapis.com/auth/drive.file'];
//const fs = require('fs');
//const credentials = require('../../credentials.json');
const { google } = require('googleapis');

const CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
const CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET;
const REDIRECT_URI = process.env.GOOGLE_REDIRECT_URI;
const TOKEN_PATH = process.env.GOOGLE_DRIVE_TOKEN;
//const TOKEN_PATH = '../../token.json';

const secret = process.env.JWT_SECRET ?? "XXX";

const { getDocumentByEmail, emailExists } = require("./ModelsHandler");
const { errorHandler } = require("./ErrorsHandler");

const decodedEmail = token => {
  try {
    const decodedToken = jwt.verify(token, secret);

    return decodedToken;
  } catch {
    throw new Error("Invalid token!");
  }
};

exports.isAuthenticated = async token => {
  try {
    const email = decodedEmail(token).email;
    await emailExists(UserModel, email);
    return true;
  } catch (err) {
    throw errorHandler("Unauthorized", err.message);
  }
};

exports.getUserAuthenticated = async token => {
  try {
    const email = decodedEmail(token).email;
    const user = await getDocumentByEmail(UserModel, email);
    return user;
  } catch (err) {
    throw errorHandler("Unauthorized", err.message);
  }
};

const getOAuthClient = () => {
  return new google.auth.OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URI);
}

 exports.getAuthorization = async () => {
  const oAuth2Client = getOAuthClient();

  // if (!fs.existsSync(TOKEN_PATH)) {
  //     const url = await oAuth2Client.generateAuthUrl({
  //         access_type: 'offline',
  //         scope: SCOPES,
  //     });
  //     return res.redirect(url);
  // }

  //const token = JSON.parse(fs.readFileSync(TOKEN_PATH));
  const token = JSON.parse(TOKEN_PATH);
  oAuth2Client.setCredentials(token);

  return oAuth2Client;
}
