const { errorHandler } = require('./ErrorsHandler');

exports.getDocumentById = async (model, id) => {
    const document = await model.findById(id);
    if (document) {
        return document;
    }
    throw errorHandler('NotFound', 'Entity with _id '+ id 
        + ' not found in ' + model.collection.collectionName);
}

exports.documentExists = async (model, id) => {
    const document = await model.exists({ _id: id });
    if (document) {
        return document;
    }
    throw errorHandler('NotFound', 'Entity with _id ' + id
        + ' not found in ' + model.collection.collectionName);
}

exports.reviewExists = async (model, id_1, id_2) => {
    const document = await model.exists({ user_id: id_1, notebook_id: id_2  });
    if (!document) {
        return true;
    }
    throw errorHandler('Conflict', 'This review already exists! ' +
         'keys: user_id: ' + id_1 + ', notebook_id: ' + id_2 );
}

exports.createDocument = async (model, document) => {
    try {
        const data = await model.create(document);
        return data;
    }
    catch (err) {
        throw errorHandler('UnprocessableEntity', err.message);
    }

}

exports.emailExists = async (model, email) => {
    const document = await model.exists({ email: email });
    if (document) {
        return document;
    }
    throw new Error('User with email ' + email + ' not found!');
}

exports.getDocumentByEmail = async (model, email) => {
    const document = await model.findOne({ email: email });
    if (document) {
        return document;
    }
    throw errorHandler('Unauthorized', 'Invalid email!');
}