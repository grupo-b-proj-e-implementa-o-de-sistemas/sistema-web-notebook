const mongoose = require("mongoose");

const MessageModel = mongoose.model("Messages");

exports.all = async (req, res, next) => {
  // #swagger.tags = ['Message']
  const data = await MessageModel.find();
  res.status(200).send(data);
};

exports.findById = async (req, res, next) => {
  // #swagger.tags = ['Message']
  const id = req.params.id;
  try {
    const data = await MessageModel.findById(id);
    res.status(200).send(data);
  } catch (err) {
    res.status(400).send(err);
  }
};

exports.create = async (req, res, next) => {
  // #swagger.tags = ['Message']
  const data = await MessageModel.create(req.body);
  res.status(201).send(data);
};

exports.deleteOne = async (req, res, next) => {
  // #swagger.tags = ['Message']
  const id = req.params.id;
  try {
    await MessageModel.findByIdAndDelete({ _id: id });

    res.status(204).send({ deleted: true });
  } catch (err) {
    res.status(400).send(err);
  }
};

exports.deleteMany = async (req, res, next) => {
  // #swagger.tags = ['Message']
  const data = JSON.parse(req.params.id);

  try {
    await data.map(async id => {
      await MessageModel.findByIdAndDelete(id.id);
    });

    res.status(204).send({ deleted: true });
  } catch (err) {
    res.status(400).send(err);
  }
};

exports.deleteAll = async (req, res, next) => {
  // #swagger.tags = ['Message']
  try {
    const data = await MessageModel.find();

    data.map(async result => {
      await MessageModel.findByIdAndDelete(result._id);
    });

    res.status(204).send({ deleted: true });
  } catch (error) {
    res.status(400).send(error);
  }
};

exports.markAsRead = async (req, res) => {
  // #swagger.tags = ['Message']
  await MessageModel.findById(req.params.id)
    .then(async result => {
      result.isRead = true;
      try {
        await MessageModel.findByIdAndUpdate(result._id, result, {
          new: true,
        });
        res.status(200).send({ updated: true });
      } catch (err) {
        res.status(400).send(err);
      }
    })
    .catch(err => {
      res.status(400).send(err);
    });
};
