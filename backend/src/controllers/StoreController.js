const mongoose = require("mongoose");

const StoreModel = mongoose.model('Stores');

const { findById } = require("../services/StoreService");

exports.findAll = async (req, res, next) => {
    // #swagger.tags = ['Store']
    const stores = await StoreModel.find();
    res.status(200).send(stores);
};

exports.findById = async (req, res, next) => {
    // #swagger.tags = ['Store']
    const id = req.params.id;

    try {
        const data = await findById(id);
        
        res.status(200).send(data);
    }
    catch (err) {
        res.status(err.status).send(err.body);
    }

};

exports.create = async (req, res, next) => {
    // #swagger.tags = ['Store']
   const store = await StoreModel.create(req.body);
   res.status(201).send(store);
};

exports.update = async (req, res, next) => {
    // #swagger.tags = ['Store']
    const id = req.params.id;
    const body = req.body;
    const notebook = await StoreModel.findByIdAndUpdate(id, body, { new: true });
    res.status(202).send(notebook);
 };

exports.deleteOne = async (req, res, next) => {
    // #swagger.tags = ['Store']
    const id = req.params.id;
    //check if this document is related first!
    // await StoreModel.deleteOne({ _id: id });
    res.status(204).send({ deleted: true });
};

exports.deleteAll = async (req, res, next) => {
    // #swagger.tags = ['Store']
    //check if documents are related first!
    // await StoreModel.deleteMany();
    // await StoreModel.collection.drop();
    res.status(204).send({ deleted: true });
};