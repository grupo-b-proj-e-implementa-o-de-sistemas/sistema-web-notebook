const mongoose = require("mongoose");
const OfferModel = mongoose.model('Offers');

const NotebookModel = mongoose.model('Notebooks');
const StoreModel = mongoose.model('Stores');

const { documentExists }  = require('../handlers/ModelsHandler');
const { errorResponse } = require('../handlers/ErrorsHandler');

exports.all = async (req, res, next) => {
    const offers = await OfferModel.find();
    res.status(200).send(offers);
};

exports.create = async (req, res, next) => {
    const offer = req.body;

    try {
        //check if composite keys exists
        await documentExists(NotebookModel, offer.notebook_id);
        await documentExists(StoreModel, offer.store_id);

        const data = await OfferModel.create(offer);
        res.status(201).send(data);
    }
    catch (err) {
        errorResponse(err, res);
    }
};

exports.update = async (req, res, next) => {
    const id = req.params.id;
    const body = req.body;
    const offer = await OfferModel.findByIdAndUpdate(id, body, { new: true });
    res.status(202).send(offer);
 };

exports.deleteAll = async (req, res, next) => {
    await OfferModel.deleteMany();
    res.status(204).send({ deleted: true });
};