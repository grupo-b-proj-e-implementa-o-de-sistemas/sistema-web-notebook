const { authenticate, checkToken, logout } = require("../services/AuthService");

module.exports = {
  async authenticate(req, res) {
     // #swagger.tags = ['Auth']
    const { email, password } = req.body;
    try {
      const { token, userDetails } = await authenticate(email, password);

      res
        .cookie("token", token, {
          httpOnly: true,
          // expiresIn: Math.floor(Date.now() + 5 * 60),
        })
        .status(200)
        .json({
          success: true,
          message: "Login successful!",
          token: token,
          user: userDetails,
        });
    } catch (err) {
      res.status(err.status).send({ success: false, ...err.body });
    }
  },
  async checkToken(req, res) {
    // #swagger.tags = ['Auth']
    try {
      await checkToken(req.params.token);

      res.status(200).send({ success: true, message: "Valid token!" });
    } catch (err) {
      res.status(err.status).send({ success: false, ...err.body });
    }
  },
  async logout(req, res) {
    // #swagger.tags = ['Auth']
    const id = req.params.id;
    try {
      await logout(id);

      res.status(200).send({ success: true, message: "Logout successful!" });
    } catch (err) {
      res.status(err.status).send({ success: false, ...err.body });
    }
  },
};
