const mongoose = require("mongoose");
const ReviewModel = mongoose.model('Reviews');

const { findByNotebookId, create } = require("../services/ReviewService");

exports.findAll = async (req, res, next) => {
    // #swagger.tags = ['Review']
    const reviews = await ReviewModel.find();
    res.status(200).send(reviews);
};

exports.findByNotebookId = async (req, res, next) => {
    // #swagger.tags = ['Review']
    const notebook_id = req.params.notebook_id;
    const token = req.query.token;

    try {
        const data = await findByNotebookId(notebook_id, token);

        res.status(200).send(data);
    }
    catch (err) {
        res.status(err.status).send(err.body);
    }
};

exports.create = async (req, res, next) => {
    // #swagger.tags = ['Review']
    const token = req.params.token;
    const insertReview = req.body;
    const notebook_id = insertReview.notebook_id;

    try {
        const data = await create(token, notebook_id, insertReview);
        
        return res.status(201).send(data);
    }
    catch (err) {
        res.status(err.status).send(err.body);
    }
};

exports.deleteOne = async (req, res, next) => {
    // #swagger.tags = ['Review']
    const id = req.params.id;
    //await ReviewModel.deleteOne({ _id: id });
    res.status(204).send({ deleted: true });
};

exports.deleteAll = async (req, res, next) => {
    //await ReviewModel.deleteMany();
    res.status(204).send({ deleted: true });
};