const formidable = require("formidable");

const {
  index,
  show,
  store,
  destroy,
  changePassword,
  update,
  verifyEmail,
  uploadImage
} = require("../services/UserService");

module.exports = {

  // Show all data in users
  async index(req, res) {
    // #swagger.tags = ['User']
    const token = req.params.token;

    try {
      const data = await index(token);

      return res.status(200).send(data);
    } catch (err) {
      res.status(err.status).send({ success: false, ...err.body });
    }
  },
  // Show a particular user by id
  async show(req, res) {
    // #swagger.tags = ['User']
    const token = req.params.token;
    const id = req.params.id;

    try {
      const data = await show(token, id);

      return res.status(200).send(data);
    } catch (err) {
      res.status(err.status).send({ success: false, ...err.body });
    }
  },
  // Store user data
  async store(req, res) {
    // #swagger.tags = ['User']
    const user = req.body;

    try {
      const data = await store(user);

      return res.status(201).send(data);
    } catch (err) {
      res.status(err.status).send({ success: false, ...err.body });
    }
  },
  // Update a user's data
  async update(req, res) {
    // #swagger.tags = ['User']
    const token = req.params.token;
    const id = req.params.id;
    const user = req.body;

    try {
      const data = await update(token, id, user);

      return res.status(202).send(data);
    } catch (err) {
      res.status(err.status).send({ success: false, ...err.body });
    }
  },
  // Delete a user
  async destroy(req, res) {
    // #swagger.tags = ['User']
    const token = req.params.token;
    const id = req.params.id;

    try {
      await destroy(token, id);

      res.status(204).send({ success: true, message: "User deleted" });
    } catch (err) {
      res.status(err.status).send({ success: false, ...err.body });
    }
  },
  async changePassword(req, res) {
    // #swagger.tags = ['User']
    const token = req.params.token;
    const newPassword = req.body.password;

    try {
      await changePassword(token, newPassword);

      return res
        .status(202)
        .send({ success: true, message: "Password has been changed!" });
    } catch (err) {
      res.status(err.status).send({ success: false, ...err.body });
    }
  },
  async verifyEmail(req, res) {
    // #swagger.tags = ['User']
    const email = req.params.email;
    try {
      await verifyEmail(email);

      return res.status(200).send({ success: true, message: "Email exists!" });
    } catch (err) {
      res.status(err.status).send({ success: false, ...err.body });
    }
  },
  async uploadImage(req, res) {
    // #swagger.tags = ['User']
    const form = formidable();
    form.parse(req, async (err, fields, files) => {
      if (err) console.log(err);
      console.log(files.file);
      try {
        const fileId = await uploadImage(files.image.name, files.image.path, files.image.type);
        const imageUrl = `https://drive.google.com/uc?export=view&id=${fileId}`;

        res.status(201).send({ uri: imageUrl });
      } catch (err) {
        console.log(err);
        res.status(400).send(err);
      }
    })
  }
};
