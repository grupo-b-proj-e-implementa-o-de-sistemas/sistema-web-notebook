const mongoose = require("mongoose");
const NotebookModel = mongoose.model('Notebooks');

const { findPaged, findById } = require("../services/NotebookService");

exports.findPaged = async (req, res, next) => {
    // #swagger.tags = ['Notebook']

    const strName = req.query.name;
    const intSize = parseInt(req.query.size);
    const intPage = parseInt(req.query.page);

    try {
        const name = strName ? strName.replace(/\s+/g, ' ').trim() : '';
        const size =  isNaN(intSize) ? 8 : intSize;
        const page =  isNaN(intPage) ? 0 : intPage;

        if((size | page) < 0)
            throw new Error('Negative values are not accepted!');

        const data = await findPaged(name, size, page);

        res.status(200).send(data);
    }
    catch (err) {
        res.status(400).send({
            error: 'Bad request',
            message: err.message
        })
    }
};

exports.findById = async (req, res, next) => {
    // #swagger.tags = ['Notebook']
    const id = req.params.id;
    try {
        const data = await findById(id);
        res.status(200).send(data);
    }
    catch (err) {
        res.status(err.status).send(err.body);
    }
};

exports.create = async (req, res, next) => {
    // #swagger.tags = ['Notebook']

    //const notebooks = req.body;
    // const data = await notebooks.reduce((prom, notebook) => (
    //     prom.then(async result => {
    //         const newNotebook = await NotebookModel.create(notebook);

    //         return result.concat(newNotebook);
    //     })
    // ), Promise.resolve([]));
   const notebook = await NotebookModel.create(req.body);
   res.status(201).send(notebook);
};

exports.update = async (req, res, next) => {
    // #swagger.tags = ['Notebook']

    const id = req.params.id;
    const body = req.body;
    const notebook = await NotebookModel.findByIdAndUpdate(id, body, { new: true });
    res.status(202).send(notebook);
 };

exports.deleteAll = async (req, res, next) => {
    // #swagger.tags = ['Notebook']
    
    //check if this document is related first!
    // await NotebookModel.deleteMany();
    // await NotebookModel.collection.drop();
    res.status(204).send({ deleted: true });
};