const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const model = mongoose.model;

const AutoIncrement = require('mongoose-sequence')(mongoose);
 
const OfferSchema = new Schema({
    _id: Number,
    notebook_id: {
        type: mongoose.Schema.Types.Number,
        ref: 'Notebooks',
        required: true,
    },
    store_id: {
        type: mongoose.Schema.Types.Number,
        ref: 'Stores',
        required: true,
    },
    price: {
        type: Number,
        required: true,
        min: [0, 'Value in price can not be negative!']
    },
    url: {
        type: String,
        required: true,
    },
});

OfferSchema.index({ notebook_id: 1, store_id: 1 }, { unique: true });

OfferSchema.pre('deleteMany', function() {
    OfferSchema.statics.counterReset('offer_id_counter', function(err) {
        // Now the counter is 0
    });
});

OfferSchema.plugin(AutoIncrement, { id: 'offer_id_counter',inc_field: '_id' });

module.exports = model('Offers', OfferSchema);
module.exports = OfferSchema;