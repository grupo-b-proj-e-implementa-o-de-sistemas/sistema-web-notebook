const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const model = mongoose.model;

const AutoIncrement = require('mongoose-sequence')(mongoose);

const reviewScore = ['VERY_BAD','BAD','MEDIUM','GOOD','GREAT'];

const ReviewSchema = new Schema({
  _id: Number,
  user_id: {
    type: mongoose.Schema.Types.Number,
    ref: 'Users',
    required: true,
  },
  notebook_id: {
    type: mongoose.Schema.Types.Number,
    ref: 'Notebooks',
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  userImg: {
    type: mongoose.Schema.Types.String,
    ref: 'Users',
  },
  score: {
   type: String,
   //enum: reviewScore,
   default: 'MEDIUM',
  },
  text: {
    type: String,
    required: false,
  },
});

ReviewSchema.index({ user_id: 1, notebook_id: 1 }, { unique: true });

ReviewSchema.pre('save', function (next) {

  if(reviewScore.includes(this.score)){
    next();
  };
  throw new Error("Value in score does not match with acceptables values: " + reviewScore);
});

ReviewSchema.pre('deleteMany', function() {
  ReviewSchema.statics.counterReset('review_id_counter', function(err) {
      // Now the counter is 0
  });
});

ReviewSchema.plugin(AutoIncrement, { id: 'review_id_counter',inc_field: '_id' });

module.exports = model('Reviews', ReviewSchema);