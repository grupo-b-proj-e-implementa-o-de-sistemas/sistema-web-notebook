const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const model = mongoose.model;

const AutoIncrement = require('mongoose-sequence')(mongoose);

const OfferSchema = require('./Offer');
//const ReviewSchema = require('./Review');
 
const NotebookSchema = new Schema({
  _id: Number,
  name: {
   type: String,
   required: true,
 },
 imgUrl: {
   type: String,
   required: true,
 },
  brand: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  color: {
    type: String,
    required: true,
  },
  screenSize: {
    type: Number,
    required: true,
  },
  processor: {
    type: String,
    required: true,
  },
  keyboard: {
    type: String,
    required: true,
  },
  stores: [
    {
      _id: {
        type: mongoose.Schema.Types.Number,
        ref: 'Store',
      },
      name: String,
      logo: String,
      offer: OfferSchema
    }
  ],
  users: [
    {
      _id: {
        type: mongoose.Schema.Types.Number,
        ref: 'Users',
      },
      review: {
        user_id: {
          type: mongoose.Schema.Types.Number,
          ref: 'Users',
        },
        notebook_id: {
          type: mongoose.Schema.Types.Number,
          ref: 'Notebooks',
        }
      }
    }
  ],
});

NotebookSchema.pre('deleteMany', function() {
  NotebookSchema.statics.counterReset('notebook_id_counter', function(err) {
      // Now the counter is 0
  });
});

NotebookSchema.plugin(AutoIncrement, { id: 'notebook_id_counter',inc_field: '_id' });
module.exports = model('Notebooks', NotebookSchema);