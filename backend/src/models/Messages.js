const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const AutoIncrement = require("mongoose-sequence")(mongoose);

// Create message schema
const MessageSchema = new Schema({
  _id: Number,
  email: {
    type: String,
    required: [true, "E-mail field is required"],
  },
  text: {
    type: String,
    required: [true, "Message field is required"],
  },
  isRead: {
    type: Boolean,
    default: false,
  },
});

MessageSchema.plugin(AutoIncrement, {
  id: "message_id_counter",
  inc_field: "_id",
});

// Name of collection in MongoDB --> Messages
module.exports = mongoose.model("Messages", MessageSchema);
