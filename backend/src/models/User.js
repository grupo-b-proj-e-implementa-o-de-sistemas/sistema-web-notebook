const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt");

const { errorHandler } = require("../handlers/ErrorsHandler");

const AutoIncrement = require("mongoose-sequence")(mongoose);

const saltRound = 10;

// Create user schema
const UserSchema = new Schema({
  _id: Number,
  name: {
    type: String,
    required: [true, "Name field is required"],
  },
  email: {
    type: String,
    unique: true,
    required: [true, "E-mail is required"],
  },
  city: {
    type: String,
    required: [true, "City is required"],
  },
  country: {
    type: String,
    required: [true, "Country is required"],
  },
  image: String,
  isAdmin: {
    type: Boolean,
    default: false,
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  dateCreated: {
    type: Date,
    default: Date.now(),
  },
  isLoggedIn: {
    type: Boolean,
    default: false,
  },
  lastLoggedIn: {
    type: Date,
  },
  lastLoggedOut: {
    type: Date,
  },
  notebooks: [
    {
      _id: {
        type: mongoose.Schema.Types.Number,
        ref: "Notebooks",
      },
      review: {
        user_id: {
          type: mongoose.Schema.Types.Number,
          ref: "Users",
        },
        notebook_id: {
          type: mongoose.Schema.Types.Number,
          ref: "Notebooks",
        },
      },
    },
  ],
});
// Encrypt password
UserSchema.methods.encrypt = async function (password) {
  const salt = await bcrypt.genSalt(parseInt(saltRound));
  const encryptedPassword = await bcrypt.hash(password, salt);
  this.password = encryptedPassword;
};

//Cryptography of password
UserSchema.pre("save", async function (next) {
  if (this.isNew || this.isModified("password")) {
    const user = this;
    try {
      await user.encrypt(user.password);
      next();
    } catch (err) {
      next(err);
    }
  }
  next();
});

//check hashed password
UserSchema.methods.isCorrectPassword = async function (password) {
  const same = await bcrypt.compare(password, this.password);
  if (!same) throw errorHandler("Unauthorized", "Incorrect password!");
};

UserSchema.plugin(AutoIncrement, { id: "user_id_counter", inc_field: "_id" });

// Name of collection in MongoDB --> users
module.exports = mongoose.model("Users", UserSchema);
