const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const model = mongoose.model;

const AutoIncrement = require('mongoose-sequence')(mongoose);

const OfferSchema = require('./Offer');
 
const StoreSchema = new Schema({
    _id: Number,
    name: {
        type: String,
        required: true,
    },
    url: {
        type: String,
        required: true,
    },
    logo: {
        type: String,
        required: true,
    },
    notebooks: [
        {
            _id: {
                type: mongoose.Schema.Types.Number,
                ref: 'Notebooks',
            },
            name: String,
            offer: OfferSchema
        },
    ],
});

StoreSchema.pre('deleteMany', function() {
    StoreSchema.statics.counterReset('store_id_counter', function(err) {
        // Now the counter is 0
    });
});

StoreSchema.plugin(AutoIncrement, { id: 'store_id_counter',inc_field: '_id' });
module.exports = model('Stores', StoreSchema);