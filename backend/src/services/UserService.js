//const mongoose = require("mongoose");

const { google } = require('googleapis');
const fs = require('fs');

const UserModel = require("../models/User");
const ReviewModel = require("../models/Review");
const NotebookModel = require("../models/Notebook");

// const UserModel = mongoose.model("Users");
// const ReviewModel = mongoose.model("Reviews");
// const NotebookModel = mongoose.model("Notebooks");

const {
  isAuthenticated,
  getUserAuthenticated,
  getAuthorization,
} = require("../handlers/AuthHandler");
const {
  getDocumentById,
  emailExists,
  createDocument,
} = require("../handlers/ModelsHandler");

const { errorHandler, errorService } = require("../handlers/ErrorsHandler");

const { passwordStrength } = require("check-password-strength");
const { defaultOptions } = require("../../checkPassword");

const UserDTO = user => ({
  _id: user._id,
  name: user.name,
});

exports.index = async token => {
  try {
    await isAuthenticated(token);

    const users = await UserModel.find();

    const data = users.map(user => UserDTO(user));

    return data;
  } catch (err) {
    throw errorService(err);
  }
};

exports.show = async (token, id) => {
  try {
    await isAuthenticated(token);

    const user = await getDocumentById(UserModel, id);

    const reviews = await ReviewModel.find({ user_id: id });

    //find user notebooks
    const notebooks = await reviews.reduce(
      (prom, review) =>
        prom.then(async result => {
          const notebook = await NotebookModel.findById(
            review.notebook_id,
            "id"
          );

          return result.concat({
            _id: notebook._id,
            review: {
              user_id: review.user_id,
              notebook_id: review.notebook_id,
            },
          });
        }),
      Promise.resolve([])
    );

    user.notebooks = notebooks;

    return user;
  } catch (err) {
    throw errorService(err);
  }
};

exports.store = async user => {
  try {
    /*
        Senha muito fraca -
        No number, lowercase, uppercase, symbols
        tamanho: 0
    
        Senha fraca -
        At least 2 of these: uppercase, lowercase, symbol and number,
        tamanho minimo: 6
        
        Senha média - 
        At least 3 of these: uppercase, lowercase, symbol and number,
        tamanho minimo: 8,
        
        Senha forte - 
        At least 4 of these: uppercase, lowercase, symbol and number,
        tamanho minimo: 8,
        */
    const password = passwordStrength(user.password, defaultOptions);
    //console.log(password);
    if (password.value === "Too weak" || password.value === "Weak")
      throw new Error(`${password.value} password!`);

    const exists = await UserModel.exists({ email: user.email });

    if (exists) {
      throw errorHandler("Conflict", "This email already exists!");
    }

    const data = await createDocument(UserModel, user);

    return data;
  } catch (err) {
    throw errorService(err);
  }
};

exports.update = async (token, id, user) => {
  try {
    await isAuthenticated(token);

    const data = await UserModel.findByIdAndUpdate(id, user, { new: true });

    return data;
  } catch (err) {
    throw err;
  }
};

exports.destroy = async (token, id) => {
  try {
    await isAuthenticated(token);

    const isExist = await ReviewModel.exists({ user_id: id });

    if(isExist)
      throw errorHandler("DatabaseIntegrity", "This document is related to other documents!");

    await UserModel.deleteOne({ _id: id });

    return true;
  } catch (err) {
    throw errorService(err);
  }
};

exports.changePassword = async (token, newPassword) => {
  try {
    const password = passwordStrength(newPassword, defaultOptions);
    //console.log(password);
    if (password.value === "Too weak" || password.value === "Weak")
      throw new Error(`${password.value} password!`);

    const user = await getUserAuthenticated(token);

    await user.encrypt(newPassword);

    await UserModel.findByIdAndUpdate(user._id, user, { new: true });

    return true;
  } catch (err) {
    throw errorService(err);
  }
};

exports.verifyEmail = async email => {
  try {
    await emailExists(UserModel, email);

    return true;
  } catch (err) {
    throw errorService(err);
  }
};

exports.uploadImage = async (fileName, filePath, mimeType, folderId) => {
  const oauthClient = await getAuthorization();

  const ID = process.env.GOOGLE_DRIVE_ID;

  const fileMetadata = { 
      name: fileName,
      parents: [ID]
  };

  const media = {
      mimeType,// "image/jpeg",
      body: fs.createReadStream(filePath)
  }

  const drive = google.drive({ version: 'v3', auth: oauthClient });

  try {
      const file = await drive.files.create({
          resource: fileMetadata,
          media: media,
      });
      
      return file.data.id;
  } catch (err) {
    //console.log(err);
    throw { status: err.code, body: err.errors[0] }
  }
}
