const mongoose = require("mongoose");

// const StoreModel = mongoose.model('Stores');
// const NotebookModel = mongoose.model('Notebooks');
const OfferModel = mongoose.model('Offers');

const StoreModel = require('../models/Store');
const NotebookModel = require('../models/Notebook');
//const OfferModel = require('../models/Offer');

const { getDocumentById }  = require('../handlers/ModelsHandler');
const { errorService } = require("../handlers/ErrorsHandler");

exports.findById = async (id) => {
    try {
        const store =  await getDocumentById(StoreModel, id);

        const offers = await OfferModel.find({ store_id: id }); 
    
        //find store notebooks
        const notebooks = await offers.reduce((prom, offer) => (
            prom.then(async result => {
                const notebook = await NotebookModel.findById(offer.notebook_id);
                return result.concat(
                    {
                        _id: notebook._id,
                        name: notebook.name,
                        offer: offer
                    },
                );
            })
        ), Promise.resolve([]));
    
        store.notebooks = notebooks;

        return store;
    }
    catch (err) {
      throw errorService(err);
    }
}