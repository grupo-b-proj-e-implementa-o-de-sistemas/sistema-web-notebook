//const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");

//const UserModel = mongoose.model("Users");
const UserModel = require('../models/User');

const secret = process.env.JWT_SECRET ?? 'XXX';

const { getDocumentByEmail, getDocumentById }  = require('../handlers/ModelsHandler');
const { errorService } = require('../handlers/ErrorsHandler');
const { isAuthenticated } = require("../handlers/AuthHandler");

exports.authenticate = async (email, password) => {
    try {
        const user = await getDocumentByEmail(UserModel, email);

        await user.isCorrectPassword(password);
  
        user.isLoggedIn = true;
        user.lastLoggedIn = Date.now();
  
        await UserModel.findByIdAndUpdate(user._id, user, { new: true });
  
        const userDetails = {
          isAdmin: user.isAdmin,
          isLoggedIn: user.isLoggedIn,
          dateCreated: user.dateCreated,
          id: user._id,
          name: user.name,
          email: user.email,
          image: user.image,
          lastLoggedIn: user.lastLoggedIn,
          lastLoggedOut: user.lastLoggedOut,
        };
  
        // Issue token
        const payload = { email };
        const token = jwt.sign(payload, secret);

        return { token, userDetails }
    }
    catch (err) {
        throw errorService(err);
    }
}

exports.checkToken = async (token) => {
    try {
        await isAuthenticated(token);
        return true;
    }
    catch (err) {
      throw errorService(err);
    }
}

exports.logout = async (id) => {
    try {
        const user = await getDocumentById(id);

        user.isLoggedIn = false;
        user.lastLoggedOut = Date.now();

        await UserModel.findByIdAndUpdate(user._id, user, { new: true });

        return true;
    }
    catch (err) {
      throw errorService(err);
    }
}