const mongoose = require("mongoose");

//const ReviewModel = mongoose.model('Reviews');
const NotebookModel = mongoose.model('Notebooks');

const ReviewModel = require('../models/Review');
//const NotebookModel = require('../models/Notebook');

const { documentExists, reviewExists, createDocument }  = require('../handlers/ModelsHandler');
const { getUserAuthenticated } = require('../handlers/AuthHandler');
const { errorService } = require("../handlers/ErrorsHandler");

exports.findByNotebookId = async (notebook_id, token) => {
    try {
         //check if notebook exists
         await documentExists(NotebookModel, notebook_id);

        const reviews = await ReviewModel.find({ notebook_id });
        
        if (!token)
            return reviews;
        
        const user = await getUserAuthenticated(token);

        const userReview = reviews.filter(item => item.user_id === user._id);

        const data = reviews.filter(item => item.user_id !== user._id);

        data.unshift(userReview[0]);

        return data;
    }
    catch (err) {
      throw errorService(err);
    }
}

exports.create = async (token, notebook_id, insertReview) => {
    try {
        const user = await getUserAuthenticated(token);
        //check if notebook exists
        await documentExists(NotebookModel, notebook_id);

        //check if review already exists
        await reviewExists(ReviewModel, user._id, notebook_id);

        const review = {
            user_id: user._id,
            username: user.name,
            userImg: user.image,
            ...insertReview
        };

        const data = await createDocument(ReviewModel, review);

        return data;
    }
    catch (err) {
      throw errorService(err);
    }
}