const mongoose = require("mongoose");

//const NotebookModel = mongoose.model('Notebooks');
const NotebookModel = require('../models/Notebook');

const OfferModel = mongoose.model('Offers');
//const StoreModel = mongoose.model('Stores');
const StoreModel = require('../models/Store');

//const ReviewModel = mongoose.model('Reviews');
const ReviewModel = require('../models/Review');
//const UserModel = mongoose.model("Users");
const UserModel = require('../models/User');

const { getDocumentById }  = require('../handlers/ModelsHandler');
const { errorService } = require("../handlers/ErrorsHandler");

const NotebookDTO = notebook => (
    {
        _id: notebook._id,
        name: notebook.name,
        imgUrl: notebook.imgUrl
    }
);

exports.findPaged = async (name, size, page) => {
    const search = { name: { $regex: name, $options:'i' } };

    const total = (name !== '')
        ? await NotebookModel.countDocuments(search)
        : await NotebookModel.estimatedDocumentCount();

    const totalPages = Math.ceil((size === 0) ? 1 : total / size);

    const skip = page * size;

    const notebooks = await NotebookModel.find(search).limit(size).skip(skip);

    const notebooksDTO = notebooks.map(notebook => NotebookDTO(notebook));

    const data = {
        content: notebooksDTO,
        size: size,
        page: page,
        totalPages: totalPages,
        totalElements: total
    }

    return data;
}

exports.findById = async (id) => {
    try {
        const notebook = await getDocumentById(NotebookModel, id);
        const offers = await OfferModel.find({ notebook_id: id });
        
        const reviews = await ReviewModel.find({ notebook_id: id });
        
        //find notebook stores
        const stores = await offers.reduce((prom, offer) => (
            prom.then(async result => {
                const store = await StoreModel.findById(offer.store_id);
                
                return result.concat(
                    {
                        _id: store._id,
                        name: store.name,
                        logo: store.logo,
                        offer: offer
                    },
                );
            })
        ), Promise.resolve([]));

        //find notebook users
        const users = await reviews.reduce((prom, review) => (
            prom.then(async result => {
                const user = await UserModel.findById(review.user_id, '_id');
                
                return result.concat(
                    {
                    _id: user._id,
                    review: {
                        user_id: review.user_id,
                        notebook_id: review.notebook_id
                    }
                    },
                );
            })
        ), Promise.resolve([]));

        notebook.stores = stores;
        notebook.users = users;

        return notebook;
    }
    catch (err) {
      throw errorService(err);
    }
}