const express = require("express");
const routes = express.Router();

routes.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

routes.get("/", (req, res) => {
  res.send("Hello World!");
});

// const { getAuthorization } = require('./handlers/AuthHandler');
// routes.post('/auth', async (req, res, next) => {
//   const oauthClient = await getAuthorization(res);
//   res.status(201).send(oauthClient);
// })

const UserController = require("./controllers/UserController");
routes.get("/users/:token", UserController.index);
routes.get("/users/:token/:id", UserController.show);
routes.post("/users", UserController.store);
routes.put("/users/:token/:id", UserController.update);
routes.delete("/users/:token/:id", UserController.destroy);
routes.post("/users/upload", UserController.uploadImage);
routes.put("/user/changePassword/:token", UserController.changePassword);
routes.get("/user/verifyEmail/:email", UserController.verifyEmail);

const AuthController = require("./controllers/AuthController");
routes.post("/user/auth/login", AuthController.authenticate);
//routes.post("/user/auth/logout/:id", AuthController.logout);
//routes.post("/user/auth/verifyToken/:token", AuthController.checkToken);


const StoreController = require("./controllers/StoreController");
routes.get("/stores", StoreController.findAll);
routes.get("/stores/:id", StoreController.findById);
//routes.post("/stores", StoreController.create);
//routes.put("/stores/:id", StoreController.update);
//routes.delete("/stores/:id", StoreController.deleteById);
//routes.delete("/stores", StoreController.deleteAll);

const NotebookController = require("./controllers/NotebookController");
routes.get("/notebooks", NotebookController.findPaged);
routes.get("/notebooks/:id", NotebookController.findById);
//routes.post("/notebooks", NotebookController.create);
//routes.put("/notebooks/:id", NotebookController.update);
//routes.delete("/notebooks", NotebookController.deleteAll);

//const OfferController = require("./controllers/OfferController");
//routes.get("/offers", OfferController.all);
//routes.post("/offers", OfferController.create);
//routes.put("/offers/:id", OfferController.update);
//routes.delete("/offers", OfferController.deleteAll);

const ReviewController = require("./controllers/ReviewController");
routes.get("/reviews", ReviewController.findAll);
routes.get(
  "/reviews/findByNotebook/:notebook_id?",
  ReviewController.findByNotebookId
);
routes.post("/reviews/:token", ReviewController.create);
//routes.delete("/reviews/:id", ReviewController.deleteOne);
//routes.delete("/reviews", ReviewController.deleteAll);

const MessageController = require("./controllers/MessageController");
routes.get("/messages", MessageController.all);
routes.get("/messages/:id", MessageController.findById);
routes.post("/messages", MessageController.create);
routes.post("/messages/markasread/:id", MessageController.markAsRead);
routes.delete("/messages/:id", MessageController.deleteOne);
routes.delete("/messages/many/:id", MessageController.deleteMany);
routes.delete("/messages", MessageController.deleteAll);

module.exports = routes;
