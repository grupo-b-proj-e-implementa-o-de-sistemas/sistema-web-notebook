//based on https://javascript.plainenglish.io/unit-testing-node-js-mongoose-using-jest-106a39b8393d
const mongoose = require("mongoose");
const result = require("dotenv").config({silent: true});
//const { MongoMemoryServer } = require("mongodb-memory-server");

//const mongod = await MongoMemoryServer.create();

const MONGO_TEST_URL = process.env.MONGO_TEST_URL;

module.exports.connect = async () => {
    //const uri = await mongod.getUri();
    //console.log(uri);
    //console.log('...connecting to database');
    const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        //poolSize: 20,
        useFindAndModify: false,
        useCreateIndex: true
    }
    await mongoose.connect(MONGO_TEST_URL, options);
}

module.exports.closeDatabase = async () => {
    //console.log('...closing database');
    //await mongoose.connection.dropDatabase();
    await mongoose.connection.close();
    //await mongoose.disconnect();
}

module.exports.clearDatabase = async () => {
    //console.log('...cleaning database');
    const collections = mongoose.connection.collections;
    for(const key in collections){
        const collection = collections[key];
        await collection.deleteMany();
        //await collection.drop();
    }
}