//const { beforeAll, afterEach, afterAll, describe } = require('jest-circus');
const { authenticate } = require('../../services/AuthService');
const { store, show, index } = require('../../services/UserService');
const db = require('../db');

beforeAll(async () => await db.connect());

afterEach(async ()=> await db.clearDatabase());

afterAll(async () => await db.closeDatabase());
  

// users for tests
const user = {
    email: 'devnote@admin.com',
    name: 'DevNote',
    password: 'Devnote123',
    city: 'Dev City',
    country: 'Dev Country'
};
const weakUser = {
    email: 'devnote@admin.com',
    name: 'DevNote',
    password: '123456e',
    city: 'Dev City',
    country: 'Dev Country'
};
const missingUser = {
    email: 'devnote@admin.com',
    password: 'Devnote123',
    city: 'Dev City',
    country: 'Dev Country'
};
// get authenticated token
const getToken = async (user) => {
    const { token } = await authenticate(user.email, user.password);
    return token;
}

// invalid token
const invalidToken = ' ';

// get id
const existingId = 1;
const nonExistingId = 1000;
const invalidId = 'a';

describe('"store" should create user document when', () => {

    test('Correct user data has been inserted', async () => {
        const data = await store(user);

        await expect(data.email).toEqual(user.email);
        await expect(data.name).toEqual(user.name);
    });
});

describe('"store" should throw error when', () => {

    test('Weak password has been inserted', async () => {
        const error = {
            name: 'BadRequest',
            status: 400,
            body: { error: 'Bad request', message: 'Weak password!' }
        }

        await expect(async () => await store(weakUser)).rejects.toEqual(error);
    });

    test('Email inserted already exists', async () => {await store(user);

        const error = {
            name: 'Conflict',
            status: 409,
            body: { error: 'Conflict', message: 'This email already exists!' }
        }

        await expect(async () => await store(user)).rejects.toEqual(error);
    });

    test('Parameter "name" is missing', async () => {const error = {
            name: 'UnprocessableEntity',
            status: 422,
            body: {
                    error: 'Validation failed',
                    message: 'Users validation failed: name: Name field is required' 
                }
            }

        await expect(async () => await store(missingUser)).rejects.toEqual(error);
    });
});

describe('"index" should return all user when', () => {

    test('Receive a valid token', async () => {
        await store(user);

        const token = await getToken(user);

        const data = await index(token);

        await expect(data).toEqual(expect.anything(expect.any([Object])));
    });
});

describe('"index" should throw unauthorized when', () => {

    test('Receive a invalid token', async () => {

        await expect(
                async () => await index(invalidToken)
            )
            .rejects.toEqual(expect.objectContaining({
                name: 'Unauthorized'
            }))
    });
});

describe('"show" should return some user data when', () => {

    test('Receive data correctly, token valid, existing user id', async () => {
        await store(user);

        const token = await getToken(user);

        const data = await show(token, existingId);

        await expect(data).toEqual(expect.objectContaining({ _id: existingId }));
    });
});

describe('"show" should throw some error when', () => {

    test('Receive invalid token, Unauthorized', async () => {
        await store(user);

        await expect(
            async () => await show(invalidToken, existingId)
        )
        .rejects.toEqual(expect.objectContaining({
            name: 'Unauthorized'
        }))

    });

    test('Receive a non existing user id, not found', async () => {
        await store(user);

        const token = await getToken(user);

        await expect(
            async () => await show(token, nonExistingId)
        )
        .rejects.toEqual(expect.objectContaining({
            name: 'NotFound'
        }))

    });

    test('Receive a not number user id, bad request', async () => {
        await store(user);

        const token = await getToken(user);

        await expect(
            async () => await show(token, invalidId)
        )
        .rejects.toEqual(expect.objectContaining({
            name: 'BadRequest'
        }))

    });
});

