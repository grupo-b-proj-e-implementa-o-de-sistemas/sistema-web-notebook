const NotebookModel = require('../../models/Notebook');
const { authenticate } = require('../../services/AuthService');
const { create, findByNotebookId } = require('../../services/ReviewService');
const { store } = require('../../services/UserService');
const db = require('../db');

beforeAll(async () => await db.connect());

afterEach(async () => await db.clearDatabase());

afterAll(async () => await db.closeDatabase());

// get id
const existingId = 1;
const nonExistingId = 1000;
const invalidId = 'a';

// user to test
const user = {
    email: 'devnote@admin.com',
    name: 'DevNote',
    password: 'Devnote123',
    city: 'Dev City',
    country: 'Dev Country'
};

//notebook to test
const notebook = {
    name: 'devNote',
    imgUrl: 'devnote.png',
    brand: 'wolf',
    type: 'example',
    color: 'gray',
    screenSize: 15.5,
    processor: 'wolf core',
    keyboard: 'shining'
};


// get authenticated token
const getToken = async (user) => {
    const { token } = await authenticate(user.email, user.password);
    return token;
}

// invalid token
const invalidToken = ' ';

//reviews to test
const review = { notebook_id: 1, score: 'GOOD', text: 'some text...' }
const reviewWithInvalidScore = { notebook_id: 1, score: '', text: 'some text...' }


describe('Create review should create review document when', () => {

    test('All resquest body is correct, valid token, notebook exists', async () => {
        await store(user);
        await NotebookModel.create(notebook);

        const token = await getToken(user);

        const data = await create(token, existingId, review);

        await expect(data).toEqual(expect.objectContaining(
            {
                user_id: existingId,
                username: expect.any(String),
                ...review
            }
        ));
    })
});

describe('Create review should throw some error when', () => {

    test('Receive an invalid token, unauthorized', async () => {
        await store(user);
        await NotebookModel.create(notebook);

        expect(
            async () => await create(invalidToken, existingId, review)
        )
        .rejects.toEqual(expect.objectContaining({ name: 'Unauthorized' }));
    });

    test('Id for notebook is not a number, bad request', async () => {
        await store(user);
        await NotebookModel.create(notebook);

        const token = await getToken(user);

        expect(
            async () => await create(token, invalidId, review)
        )
        .rejects.toEqual(expect.objectContaining({ name: 'BadRequest' }));
    });

    test('Notebook id does not exists, not found', async () => {
        await store(user);
        await NotebookModel.create(notebook);

        const token = await getToken(user);

        expect(
            async () => await create(token, nonExistingId, review)
        )
        .rejects.toEqual(expect.objectContaining({ name: 'NotFound' }));
    });

    test('Review composite keys already exists, Conflict', async () => {
        await store(user);
        await NotebookModel.create(notebook);

        const token = await getToken(user);

        // fist review 
        await create(token, existingId, review);

        expect(
            // second review with same keys (user id and notebook id)
            async () => await create(token, existingId, review)
        )
        .rejects.toEqual(expect.objectContaining({ name: 'Conflict' }));
    });

    test('Score does not have a valid value, UnprocessableEntity', async () => {
        await store(user);
        await NotebookModel.create(notebook);

        const token = await getToken(user);

        expect(
            async () => await create(token, existingId, reviewWithInvalidScore)
        )
        .rejects.toEqual(expect.objectContaining({ name: 'UnprocessableEntity' }));
    });
});

describe('findByNotebookId should return reviews when', () => {
  
    test('Notebook exists, user is not logged in', async () => {
        await NotebookModel.create(notebook);

        const data = await findByNotebookId(existingId);

        await expect(data).toEqual(expect.anything(expect.any([])));
    });

    test('Notebook exists, user is logged in but no have own review', async () => {
        await NotebookModel.create(notebook);

        await store(user);

        const token = await getToken(user);

        const data = await findByNotebookId(existingId, token);

        await expect(data).toEqual(expect.anything(expect.any([
            expect.not.objectContaining(
                { user_id: existingId }
            )
        ])));
    });

    test('Notebook exists, user is logged and have own review', async () => {
        await NotebookModel.create(notebook);

        await store(user);

        const token = await getToken(user);

        await create(token, existingId, review);

        const data = await findByNotebookId(existingId, token);

        await expect(data).toEqual(expect.anything(expect.any([
            expect.objectContaining(
                { user_id: existingId }
            )
        ])));
    });
});

describe('findByNotebookId should throw some error when', () => {

    test('id received is not a number, bad request', async () => {

        await expect(
            async () => await findByNotebookId(invalidId)
        )
        .rejects.toEqual(expect.objectContaining({ name: 'BadRequest' }));
    });
  
    test('notebook id does not exists, not found', async () => {
        await NotebookModel.create(notebook);

        await expect(
            async () => await findByNotebookId(nonExistingId)
        )
        .rejects.toEqual(expect.objectContaining({ name: 'NotFound' }));
    });

    test('Have a invalid token, Unauthorized', async () => {
        await NotebookModel.create(notebook);

        await expect(
            async () => await findByNotebookId(existingId, invalidToken)
        )
        .rejects.toEqual(expect.objectContaining({ name: 'Unauthorized' }));
    });
});