const { authenticate } = require('../../services/AuthService');
const { store } = require('../../services/UserService');
const db = require('../db');

beforeAll(async () => await db.connect());

afterEach(async ()=> await db.clearDatabase());

afterAll(async () => await db.closeDatabase());

//users for test
const user = {
    email: 'devnote@admin.com',
    name: 'DevNote',
    password: 'Devnote123',
    city: 'Dev City',
    country: 'Dev Country'
};

describe('User can authenticate when', () => {

    test('Insert authentication data correctly', async () => {
        await store(user);

        const { token, userDetails } = await authenticate(user.email, user.password);

        await expect(token).toEqual(expect.any(String));
        await expect(userDetails).toEqual(expect.any(Object));
    });
});

describe('User is not authorized when', () => {

    test('Insert invalid password', async () => {
        await store(user);

        const invalidPassword = '';

        await expect(
            async () => await authenticate(user.email, invalidPassword)
        )
        .rejects.toEqual(expect.objectContaining({
            name: 'Unauthorized'
        }))
    });

    test('Insert invalid email', async () => {
        const invalidEmail = '';

        await expect(
            async () => await authenticate(invalidEmail, user.password)
        )
        .rejects.toEqual(expect.objectContaining({
            name: 'Unauthorized'
        }))
    });
    
    test('Password missing throwing bad request', async () => {
        await store(user);

        await expect(
            async () => await authenticate(user.email, null)
        )
        .rejects.toEqual(expect.objectContaining({
            name: 'BadRequest'
        }))
    });
});