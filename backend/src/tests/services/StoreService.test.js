const StoreModel = require('../../models/Store');
const { findById } = require('../../services/StoreService');
const db = require('../db');

beforeAll(async () => await db.connect());

afterEach(async ()=> await db.clearDatabase());

afterAll(async () => await db.closeDatabase());

//store for test
const store = { name: 'devNote', url: 'devNote.com' , logo: 'store_logo'}

// get id
const existingId = 1;
const nonExistingId = 1000;
const invalidId = 'a';

describe('findById should return store when', () => {

    test('Store id exists', async () => {
        await StoreModel.create(store);

        const data = await findById(existingId);

        await expect(data).toEqual(expect.objectContaining({ _id: existingId, ...store }));
    });
});

describe('findById should throw some error when', () => {

    test('Store id does not exists, not found', async () => {
        await expect(
            async () => await findById(nonExistingId)
        )
        .rejects.toEqual(expect.objectContaining({
            name: 'NotFound'
        }))
    });

    test('Store id have invalid type, bad request', async () => {
        await expect(
            async () => await findById(invalidId)
        )
        .rejects.toEqual(expect.objectContaining({
            name: 'BadRequest'
        }))
    });
});
