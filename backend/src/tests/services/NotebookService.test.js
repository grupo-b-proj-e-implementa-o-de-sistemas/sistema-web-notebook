const NotebookModel = require('../../models/Notebook');
const { findPaged, findById } = require('../../services/NotebookService');
const db = require('../db');

beforeAll(async () => await db.connect());

afterEach(async () => await db.clearDatabase());

afterAll(async () => await db.closeDatabase());

//notebooks to test
const notebook_1 = {
    name: 'devNote',
    imgUrl: 'devnote.png',
    brand: 'wolf',
    type: 'example',
    color: 'gray',
    screenSize: 15.5,
    processor: 'wolf core',
    keyboard: 'shining'
};

//default optionals paramenters
const defaultName = '';
const defaultSize = 8;
const defaultPage = 0;

// get id
const existingId = 1;
const nonExistingId = 1000;
const invalidId = 'a';

describe('findPaged should return notebooks paged when', () => {

    test('Does not have any optional paramenter, setting default', async () => {
        const data = await findPaged(defaultName, defaultSize, defaultPage);

        await expect(data).toEqual(expect.anything(
            expect.objectContaining(
                {
                    content: expect.any([]),
                    size: defaultSize,
                    page: defaultPage,
                    totalPages: 1,
                    totalElements: 0
                }
            )
        ));
    });
});

describe('findById should return notebook data when', () => {

    test('Id exists', async () => {
        await NotebookModel.create(notebook_1);

        const data = await findById(existingId);

        await expect(data).toEqual(expect.objectContaining({ _id: existingId }));
    });
});

describe('findById should throw some error when', () => {

    test('Id does not exists, not found', async () => {
        await expect(
            async () => await await findById(nonExistingId)
        )
        .rejects.toEqual(expect.objectContaining({ name: 'NotFound' }));
    });

    test('Id is not a number, bad request', async () => {
        await expect(
            async () => await await findById(invalidId)
        )
        .rejects.toEqual(expect.objectContaining({ name: 'BadRequest' }));
    });
});